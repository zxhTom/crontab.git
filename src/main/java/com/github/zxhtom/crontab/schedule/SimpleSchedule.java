package com.github.zxhtom.crontab.schedule;

import com.github.zxhtom.crontab.mapper.TestMapper;
import com.github.zxhtom.crontab.model.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.List;

/**
 * @package com.github.zxhtom.crontab.schedule
 * @Class SimpleSchedule
 * @Description 定时器
 * @Author zhangxinhua
 * @Date 19-11-15 上午9:59
 */
@Component
public class SimpleSchedule {

    @Autowired
    TestMapper testMapper;

    //@Scheduled(cron = "*/6 * * * * ?")
    private void process() {
        List<Test> tests = testMapper.getTests();
        System.out.println("通过定时器查询数据："+new Date()+"===" +tests);
    }
}
