package com.github.zxhtom.crontab.config.v2;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

/**
 * @package com.github.zxhtom.crontab.config.v2
 * @Class ScheduleConfig
 * @Description TODO
 * @Author zhangxinhua
 * @Date 19-11-15 上午11:04
 */
@Configuration
public class ScheduleConfig {

    /**
     * 线程池
     * @return
     */
    @Bean
    public TaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        //定时任务执行线程池核心线程数
        taskScheduler.setPoolSize(5);
        taskScheduler.setRemoveOnCancelPolicy(true);
        taskScheduler.setThreadNamePrefix("taskSchedulerThreadPool-");
        return taskScheduler;
    }
}
