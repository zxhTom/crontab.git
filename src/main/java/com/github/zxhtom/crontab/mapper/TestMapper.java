package com.github.zxhtom.crontab.mapper;

import com.github.zxhtom.crontab.model.Test;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @package com.github.zxhtom.crontab.mapper
 * @Class TestMapper
 * @Description TODO
 * @Author zhangxinhua
 * @Date 19-11-15 上午9:37
 */
@Mapper
public interface TestMapper {

    /**
     * 获取数据集合
     * @return
     */
    @Select("select * from Test")
    public List<Test> getTests();

}
